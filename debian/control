Source: golang-github-makeworld-the-better-one-dither
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-stretchr-testify-dev
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-makeworld-the-better-one-dither
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-makeworld-the-better-one-dither.git
Homepage: https://github.com/makeworld-the-better-one/dither
XS-Go-Import-Path: github.com/makeworld-the-better-one/dither

Package: golang-github-makeworld-the-better-one-dither-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: fast, correct image dithering library in Go
 dither is a library for dithering images in Go.  It has many dithering
 algorithms built-in, and allows you to specify your own.  Correctness
 is a top priority, as well as performance.  It is designed to work well
 on its own, but also implements interfaces from the standard library,
 so that it can be integrated easily in a wide variety of situtations.
 .
 This library is uniquely correct from a math and quality perspective.
 It linearizes the image, and color comparisons are done with human
 luminance perception in mind (channel weighting).  Few-to-no other
 libraries do this.
 .
 It supports images that make use of the alpha channel, AKA transparency.
 .
 Types of dithering supported:
 .
  * Random noise (in grayscale and RGB)
  * Ordered Dithering
    - Bayer matrix of any size (as long as dimensions are powers of two)
    - Clustered-dot - many different preprogrammed matrices
    - Some unusual horizontal or vertical line matrices
    - Yours?
      + Using PixelMapperFromMatrix, this library can dither using
        any matrix
      + If you need more freedom, PixelMapper can be used to implement
        any method of dithering that affects each pixel individually
 .
  * Error diffusion dithering
    - Simple 2D
    - Floyd-Steinberg, False Floyd-Steinberg
    - Jarvis-Judice-Ninke
    - Atkinson
    - Stucki
    - Burkes
    - Sierra/Sierra3, Sierra2, Sierra2-4A/Sierra-Lite
    - Steven Pigeon (https://hbfs.wordpress.com/2013/12/31/dithering/)
    - Yours? Custom error diffusion matrices can be used by the library.
 .
 More methods of dithering are being worked on, such as Riemersma,
 Yuliluoma, and blue noise.
